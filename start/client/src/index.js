import React from 'react';
import ReactDom from 'react-dom';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { Query, ApolloProvider } from 'react-apollo';
import gql from "graphql-tag";

import Pages from './pages';
import Login from './pages/login';

const IS_LOGGED_IN = gql`
    query isUserLoggedIn {
        isLoggedIn @client
    }
`;
const cache = new InMemoryCache();
const link = new HttpLink({
  uri: 'https://server-l6uvj33we.now.sh/src/',
  headers: {
    authorization: localStorage.getItem('token'),
  },
})

const client = new ApolloClient({
  cache,
  link,
})

cache.writeData({
    data: {
        isLoggedIn: !!localStorage.getItem('token'),
        cartItems: [],
      },
});
/*client
  .query({
    query: gql`
      query GetLaunch {
        launch(id: 56) {
          id
          mission {
            name
          }
        }
      }
    `
  })
  .then(result => console.log(result));
*/
  ReactDom.render(
      <ApolloProvider client = {client}>
        <Query query={ IS_LOGGED_IN }>
          { ({data}) => ( data.isLoggedIn?<Pages />:<Login />) }
        </Query>
      </ApolloProvider>, document.getElementById('root')
  );