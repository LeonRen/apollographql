const { paginateResults } = require('./utils');

module.exports = {
    Query: {
        launches: async (_,{ pageSize=20, after },{ dataSources }) => {
            const allLaunches = await dataSources.launchAPI.getAllLaunches();
            allLaunches.reverse();
            const launches = paginateResults({
                after,
                pageSize,
                results: allLaunches,
            });

            return {
                launches,
                cursor: launches.length ? launches[launches.length -1].cursor : null,
                // if the cursor of the end of the paginated results is the same as the 
                // last item in _all_ results, then there are no more results after this
                hasMore: launches.length ? launches[ launches.length -1 ].cursor !== allLaunches[allLaunches.length -1 ].cursor : false
            };
        },
        launch: (_, { id }, { dataSources }) => dataSources.launchAPI.getLaunchById({ launchId: id }),
        me: async (_, __, { dataSources }) => dataSources.userAPI.findOrCreateUser(),

    },

    Mutation: {
        bookTrips: async (_, { launchIds }, { dataSources }) => {
            const trips = await dataSources.userAPI.bookTrips( { launchIds });
            const launches = await dataSources.launchAPI.getLaunchByIds( { launchIds });
            
            return {
                success: trips && trips.length === launchIds.length,
                message: trips && trips.length === launchIds.length ? "All Trips are booked successfully" : `The following trips 
                could not be booked ${launchIds.filter(id => { console.log(id);return !trips.include(id);})}`,
                launches: launches
            };
            
        },

        login: async (_, { email }, { dataSources } ) => {
            const keys = Object.keys(dataSources.userAPI);
            //const user = await dataSources.userAPI.findOrCreateUser( { email });
            console.log(keys);
            const userAPI = dataSources.userAPI;
            console.log(userAPI.context.dataSources);
            //return user.token;
            
        }
    
    },

    Rocket: {
        id: async (rocket, __ , { dataSources }) => {
             return rocket.id;
        }


    }
}